
SDA - ANOMALIA DE DEFINI��O DE ESTADO
� uma anomalia que ocorre quando as intera��es de estado de um descendente n�o s�o consistentes com as do ancestral, ou seja, quando os m�todos da classe herdeira n�o fornecem as mesmas defini��es para as vari�veis de estado dos m�todos da classe herdada que foram sobrescritos.
SA�DA: Foram comparados todos os m�todos polim�rficos existentes entre si, os que apresentam diverg�ncias quanto �s defini��es ou chamadas de fun��es ser�o exibidos abaixo.
TESTE: Os seguintes m�todos podem apresentar esta anomalia entre si e devem ser testados a fim de verificar as intera��es de estado.

------------------------------------------------------------------------------------------

Poss�vel SDA nos seguintes metodos polimorficos entre si: 
_ZNK5Sopro5tocarE4nota _ZNK9Percussao5tocarE4nota _ZNK9ComCordas5tocarE4nota _ZNK6Metais5tocarE4nota _ZNK8Madeiras5tocarE4nota 
Os metodos diferem quanto as DEFINI��ES, por favor conferir as informacoes abaixo: 
	Metodo: _ZNK5Sopro5tocarE4nota
		Tipo: Defini��o -    	Atributo: tipo   	Objeto_nome: this_value   	Objeto_classe: Sopro
	Metodo: _ZNK9Percussao5tocarE4nota
		Tipo: Defini��o -    	Atributo: tipo   	Objeto_nome: this_value   	Objeto_classe: Percussao
	Metodo: _ZNK9ComCordas5tocarE4nota
		Tipo: Defini��o -    	Atributo: tipo   	Objeto_nome: this_value   	Objeto_classe: ComCordas
	Metodo: _ZNK6Metais5tocarE4nota
		Tipo: Defini��o -    	Atributo: tipo   	Objeto_nome:    	Objeto_classe: 
		Tipo: Defini��o -    	Atributo: categoria   	Objeto_nome: this_value   	Objeto_classe: Metais
	Metodo: _ZNK8Madeiras5tocarE4nota
		Tipo: Defini��o -    	Atributo: tipo   	Objeto_nome:    	Objeto_classe: 
		Tipo: Defini��o -    	Atributo: categoria   	Objeto_nome: this_value   	Objeto_classe: Madeiras
------------------------------------------------------------------------------------------

Poss�vel SDA nos seguintes metodos polimorficos entre si: 
_ZN5Sopro7ajustarEi _ZN9Percussao7ajustarEi _ZN9ComCordas7ajustarEi _ZN6Metais7ajustarEi _ZN8Madeiras7ajustarEi 
Os metodos diferem quanto as DEFINI��ES, por favor conferir as informacoes abaixo: 
	Metodo: _ZN5Sopro7ajustarEi
		Tipo: Defini��o -    	Atributo: ajuste   	Objeto_nome: this_value   	Objeto_classe: Sopro
	Metodo: _ZN9Percussao7ajustarEi
		Tipo: Defini��o -    	Atributo: ajuste   	Objeto_nome: this_value   	Objeto_classe: Percussao
	Metodo: _ZN9ComCordas7ajustarEi
		Tipo: Defini��o -    	Atributo: ajuste   	Objeto_nome: this_value   	Objeto_classe: ComCordas
	Metodo: _ZN6Metais7ajustarEi
		Tipo: Defini��o -    	Atributo: ajuste   	Objeto_nome:    	Objeto_classe: 
	Metodo: _ZN8Madeiras7ajustarEi
		Tipo: Defini��o -    	Atributo: ajuste   	Objeto_nome:    	Objeto_classe: 
